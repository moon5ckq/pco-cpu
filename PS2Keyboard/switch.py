

t = [['A', '1C', 'F0', '1C'], ['B', '32', 'F0', '32'], ['C', '21', 'F0', '21'], ['D', '23', 'F0', '23'], ['E', '24', 'F0', '24'], ['F', '2B', 'F0', '2B'], ['G', '34', 'F0', '34'], ['H', '33', 'F0', '33'], ['I', '43', 'F0', '43'], ['J', '3B', 'F0', '3B'], ['K', '42', 'F0', '42'], ['L', '4B', 'F0', '4B'], ['M', '3A', 'F0', '3A'], ['N', '31', 'F0', '31'], ['O', '44', 'F0', '44'], ['P', '4D', 'F0', '4D'], ['Q', '15', 'F0', '15'], ['R', '2D', 'F0', '2D'], ['S', '1B', 'F0', '1B'], ['T', '2C', 'F0', '2C'], ['U', '3C', 'F0', '3C'], ['V', '2A', 'F0', '2A'], ['W', '1D', 'F0', '1D'], ['X', '22', 'F0', '22'], ['Y', '35', 'F0', '35'], ['Z', '1A', 'F0', '1A'], ['0', '45', 'F0', '45'], ['1', '16', 'F0', '16'], ['2', '1E', 'F0', '1E'], ['3', '26', 'F0', '26'], ['4', '25', 'F0', '25'], ['5', '2E', 'F0', '2E'], ['6', '36', 'F0', '36'], ['7', '3D', 'F0', '3D'], ['8', '3E', 'F0', '3E'], ['9', '46', 'F0', '46'], ['`', '0E', 'F0', '0E'], ['-', '4E', 'F0', '4E'], ['=', '55', 'F0', '55'], ['\\', '5D', 'F0', '5D'], ['[', '54', 'F0', '54'], [']', '58', 'F0', '58'], [';', '4C', 'F0', '4C'], ["'", '52', 'F0', '52'], [',', '41', 'F0', '41'], ['.', '49', 'F0', '49'], ['/', '4A', 'F0', '4A'], ['SPACE', '29', 'F0', '29'], ['ENTER', '5A', 'F0', '5A']]


def change(ch):
	if ch == 'SPACE':
		return 0
	elif ch == 'ENTER':
		return 30
	elif ord(ch[0]) >= 48 and ord(ch[0]) <= 57:
		return ord(ch[0])
	elif ord(ch[0]) >= 65 and ord(ch[0]) <= 90:
		return ord(ch[0]) - 65 + 1
	elif ch == ',':
		return 35
	elif ch == '.':
		return 36
	elif ch == '=':
		return 37
	elif ch == '+':
		return 38
	elif ch == '-':
		return 39
	else:
		return "none"

def change2bin6(n):
	s = []
	for i in range(0, 6):
		s.append(n%2)
		n = n / 2
	ss = []
	for i in range(0, 6):
		ss.append(str(s[5-i]))
	return "".join(ss)

def change2bin8(d):
	s = ""
	for i in range (0 ,2):
		if ord(d[i]) < 65:
			s =  s + change2bin6(ord(d[i]) - 48)[2:6]
		else:
			s =  s + change2bin6(ord(d[i]) - 65 + 10)[2:6]
	return s


for i in t:
	if change(i[0]) != "none" :
		#print i[0], change2bin8(i[1]) , change2bin6(change(i[0]))
		print '"'+str(change2bin6(change(i[0])))+'" when "'+str(change2bin8(i[1]))+'" , '
