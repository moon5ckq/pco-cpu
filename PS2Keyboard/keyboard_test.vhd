------------------------------------
-- 2012-11-25 11:58:33
-- Author : Keqing Chen
-- keyboard_test
------------------------------------
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity keyboard_test is
	port(
		ps2data, ps2clk, clk_right, rst	:	in std_logic;
		oLed	:	in std_logic_vector (15 downto 0)
	);
end keyboard_test;

architecture main of keyboard_test is
component Keyboard is
	port (
		datain, clkin	: 	in std_logic ; -- PS2 clk and data
		fclk, rst 	: 	in std_logic ;  -- filter clock
	--	fok : out std_logic ;  -- data output enable signal
		scancode 	: 	out std_logic_vector(7 downto 0) -- scan code signal output
	);
end component ;

	signal scancode	: 	std_logic_vector(7 downto 0);
	signal rst_in 	: 	std_logic;
	signal clk_f 	: 	std_logic;

begin
	rst_in <= not rst;

	Keyboard_port :	Keyboard port map (
		datain => ps2data,
		clkin => ps2clk,
		rst => rst_in,
		fclk => clk_right,
		scancode => oLed(7 downto 0)
	);

	oLed(15 downto 8) <= (others => '0');

end architecture;

